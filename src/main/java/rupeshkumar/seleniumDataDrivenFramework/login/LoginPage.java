package rupeshkumar.seleniumDataDrivenFramework.login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import rupeshkumar.seleniumDataDrivenFramework.base.BaseClass;

public class LoginPage extends BaseClass
{
	protected WebDriver driver;
	
	//declaration stage 
	@FindBy(xpath = "//input[@name=\"uid\"]")
	private WebElement username;
	
	@FindBy(xpath = "//input[@name=\"password\"]")
	private WebElement password;
	
	@FindBy(xpath = "//input[@name=\"btnLogin\"]")
	private WebElement loginBtn;
	
	//in
	public LoginPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	//utilization
	public void login(String pUsername,String pPassword)
	{
		username.sendKeys(pUsername);
		password.sendKeys(pPassword);
		loginBtn.click();
	}
}
