package rupeshkumar.seleniumDataDrivenFramework.base;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;
import rupeshkumar.seleniumDataDrivenFramework.screenshot.Screenshot;
import rupeshkumar.seleniumDataDrivenFramework.testData.ConfigData;

public class BaseClass
{
	protected static WebDriver driver;		
	protected String browserName =  ConfigData.getProperty("browserName");	
	protected String url = ConfigData.getProperty("url");	

	@BeforeMethod
	public void launchApplication()
	{
		System.out.println(browserName);
		switch (browserName)
		{
		case "chrome":
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			break;
		case "firefox":
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			break;
		case "edge":
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
			break;
		default:
			break;
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.manage().deleteAllCookies();
		driver.get(url);
	}

	@AfterMethod
	public void closeApplication(ITestResult result)
	{
		if (ITestResult.FAILURE == result.getStatus())
		{
			Screenshot.takeScreenshotForFailedTestcases(driver, result.getName());
		}
		driver.quit();
	}
}
