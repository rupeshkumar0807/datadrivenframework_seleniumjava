package rupeshkumar.seleniumDataDrivenFramework.screenshot;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import rupeshkumar.seleniumDataDrivenFramework.base.BaseClass;

public class Screenshot extends BaseClass
{
	static WebDriver driver;
	protected static final String USER_DIRECTORY = System.getProperty("user.dir")+"\\Screenshots";
	
	
	public Screenshot(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public static void takeScreenshotForFailedTestcases(WebDriver driver,String screenshotName)
	{
		File image = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String path = USER_DIRECTORY+screenshotName+".png";
		File file = new File(path);
		try
		{
			FileHandler.copy(image, file);
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
